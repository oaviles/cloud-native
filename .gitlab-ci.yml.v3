# This file is a template, and might need editing before it works on your project.
# To contribute improvements to CI/CD templates, please follow the Development guide at:
# https://docs.gitlab.com/ee/development/cicd/templates.html
# This specific template is located at:
# https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Docker.gitlab-ci.yml

# Build a Docker image with CI/CD and push to the GitLab registry.
# Docker-in-Docker documentation: https://docs.gitlab.com/ee/ci/docker/using_docker_build.html
#
# This template uses one generic job with conditional builds
# for the default branch and all other (MR) branches.

variables:
  CS_ANALYZER_IMAGE: "$CI_TEMPLATE_REGISTRY_HOST/security-products/container-scanning:5"

build-and-push-image:
  # Use the official docker image.
  image: docker:latest
  stage: build
  services:
    - docker:dind
  before_script:
    - docker login -u "$REGISTRY_USER" -p "$REGISTRY_PASSWORD" $REGISTRY
  # Default branch leaves tag empty (= latest tag)
  # All other branches are tagged with the escaped branch name (commit ref slug)
  script:
    - |
      if [[ "$CI_COMMIT_BRANCH" == "$CI_DEFAULT_BRANCH" ]]; then
        tag=""
        echo "Running on default branch '$CI_DEFAULT_BRANCH': tag = 'latest'"
      else
        tag=":$CI_COMMIT_REF_SLUG"
        echo "Running on branch '$CI_COMMIT_BRANCH': tag = $tag"
      fi
    - docker build --pull -t "$REGISTRY_IMAGE${tag}" .
    - docker push "$REGISTRY_IMAGE${tag}"
  # Run this job in a branch where a Dockerfile exists
  rules:
    - if: $CI_COMMIT_BRANCH
      exists:
        - Dockerfile

container_scanning:
  image: "$CS_ANALYZER_IMAGE$CS_IMAGE_SUFFIX"
  stage: test
  variables:
    # To provide a `vulnerability-allowlist.yml` file, override the GIT_STRATEGY variable in your
    # `.gitlab-ci.yml` file and set it to `fetch`.
    # For details, see the following links:
    # https://docs.gitlab.com/ee/user/application_security/container_scanning/index.html#overriding-the-container-scanning-template
    # https://docs.gitlab.com/ee/user/application_security/container_scanning/#vulnerability-allowlisting
    GIT_STRATEGY: none
  allow_failure: true
  artifacts:
    reports:
      container_scanning: gl-container-scanning-report.json
      dependency_scanning: gl-dependency-scanning-report.json
    paths: [gl-container-scanning-report.json, gl-dependency-scanning-report.json]
  dependencies: []
  script:
    - gtcs scan
  rules:
    - if: $CONTAINER_SCANNING_DISABLED
      when: never
    - if: $CI_COMMIT_BRANCH &&
          $CI_GITLAB_FIPS_MODE == "true" &&
          $CS_ANALYZER_IMAGE !~ /-(fips|ubi)\z/
      variables:
        CS_IMAGE_SUFFIX: -fips
    - if: $CI_COMMIT_BRANCH


deploy-api-image:
  image: mcr.microsoft.com/azure-cli
  stage: deploy
  before_script:
    - az login --service-principal -u $SP_USER -p $SP_PASSWORD --tenant $AZURE_TENANT
    - az aks install-cli
    - az aks get-credentials --resource-group $AZURE_RG --name $CLUSTER_NAME
  script:
    - kubectl create -f ./yaml_files/deploy-microservice.yaml
    - kubectl get service -n api
  environment:
    name: production
    url: http://$IP_SERVICE/weatherforecast
